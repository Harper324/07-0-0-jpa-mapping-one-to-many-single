package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class PersonAndOfficeTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    EntityManager entityManager;

    public void flushAndClear(Runnable run) {
        run.run();
        entityManager.flush();
        entityManager.clear();
    }


    /*
    person控制关系，office控制级联
    save profile then account will be saved,
    but save account profile will not be saved
    */
    @Test
    void should_not_save_office_when_save_person() {
        flushAndClear(() -> {
            Person person = new Person("aaa");
            Office xian = new Office("Xi'an");
            xian.addPerson(person);
            personRepository.save(person);

        });
        assertEquals(1L, personRepository.findAll().get(0).getId().longValue());
//        error! can not find office
        assertEquals(1L, officeRepository.findAll().get(0).getId().longValue());
    }

    @Test
    void should_save_account_when_save_profile() {
        flushAndClear(() -> {
            Person person = new Person("aaa");
            Office xian = new Office("Xi'an");
            xian.addPerson(person);
            officeRepository.save(xian);

        });
        assertEquals(1L, officeRepository.findAll().get(0).getId().longValue());
        assertEquals(1L, personRepository.findAll().get(0).getId().longValue());
    }

    @Test
    void should_delete_person_when_delete_city() {
        flushAndClear(() -> {
            Person person = new Person("aaa");
            Office xian = new Office("Xi'an");
            xian.addPerson(person);
            officeRepository.save(xian);
            officeRepository.deleteById(1L);

        });
        assertEquals(0, officeRepository.findAll().size());
        assertEquals(0, personRepository.findAll().size());
    }


    @Test
    void should_not_delete_city_when_delete_person() {
        flushAndClear(() -> {
            Person person = new Person("aaa");
            Office xian = new Office("Xi'an");
            xian.addPerson(person);
            officeRepository.save(xian);
            personRepository.deleteById(1L);

        });
//        error! can not delete person
        assertEquals(0, officeRepository.findAll().size());
        assertEquals(0, personRepository.findAll().size());
    }
}
