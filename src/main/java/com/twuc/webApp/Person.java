package com.twuc.webApp;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 9)
    private String name;

//    @Column
//    private Office office;

    public Person() {
    }

//    public Person(String name, Office office) {
//        this.name = name;
//        this.office = office;
//    }

    public Person(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

//    public Office getOffice() {
//        return office;
//    }
//
//    public void setOffice(Office office) {
//        if (this.office != null && this.office.equals(office)) {
//            return;
//        }
//        this.office = office;
//        this.office.addPerson(this);
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) return true;
//        if (obj == null || getClass() != obj.getClass()) return false;
//        Person person = (Person) obj;
//        return Object.equals(id, person.id);
//    }


}


