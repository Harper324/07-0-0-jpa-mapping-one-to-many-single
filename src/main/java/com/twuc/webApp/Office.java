package com.twuc.webApp;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Office {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name="people")
    private List<Person> people = new ArrayList<>();

    public Office(String name, List<Person> people) {
        this.name = name;
        this.people = people;
    }

    public Office() {
    }

    public Office(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }

    public void addPerson(Person person) {

        this.people.add(person);
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) return true;
//        if (obj == null || getClass() != obj.getClass()) return false;
//        Office office = (Office) obj;
//        return Object.equals(id, office.id);
//    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
